const request = require("request-promise").defaults({json: true});

const _HOST = "https://nhentai.net/";

/* https://nhentai.net/api/galleries/search?query=naruto&page=2
https://nhentai.net/api/galleries/all?page=3
https://nhentai.net/api/galleries/tagged?tag_id=123&page=2
https://nhentai.net/api/gallery/123 */

module.exports = {
    getPage : function(page_number,callback){
        request.get(_HOST+"api/galleries/all?page="+page_number)
            .then(callback);
    },
    feed : function(limit = 10){
        let page = 1;
        let count = 0;
        this.getPage(page,(json => {
            if (json.result.length > 0) {
                console.log(json);
            }
        }));
    },
    search: function(keyword,tags,page,favorites){ // options [page, to_page]. [1,100]
        return new Promise((resolve) => {
            let data = [];
        
            if (keyword !== undefined) {
                //console.log(_HOST+"api/galleries/search?query="+keyword+"&page="+page);
                request.get(_HOST+"api/galleries/"+((keyword === "all") ? "all?" : "search?query="+keyword)+"&page="+page).then(json => {
                    if (json.error !== true) {
                        json.result.map(el => {
                            if (this.checkTags(el, tags) === true || tags === []) {
                                if (favorites === undefined) favorites = 0;

                                if (el.num_favorites >= favorites) data.push({id: el.id, media_id: el.media_id, title: el.title.english});
                            }
                        });
                        resolve(data);
                    }
                }).catch((error) => { console.log("request error ! - " + page) });

            } else console.log("something went wrong");

        });
    },
    checkTags: function(post,tags){
            let match = false;
        
            if (typeof tags == "object") {
                for(i=0;i<tags.length;++i) {
                    if (this.getTags(post).includes(tags[i]) !== true ||this.getTags(post).includes("males only") === true) return false;
                }
                return true;
            } else console.log("something went wrong");
    },
    getTags: function(post){

            let tags = [];

            if ( typeof post == "object") {
                post.tags.map(el => {
                    /* if (el.type == "tag" ) */ tags.push(el.name);
                });
    
                return tags
    
            } else console.log("something went wrong");
    },
    findPostByID: function(id){
        return new Promise((resolve) => {
            request.get(_HOST+"api/gallery/"+id).then(resolve);
        });
    }
    
}
